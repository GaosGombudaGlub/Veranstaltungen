## Veranstaltungsrepo

Infos, Skripte, Materialien zu den GGG-Veranstaltungen aka dem Cybermittwoch*.

Termine sind auf https://gaosgombudaglub.codeberg.page zu finden.

<sup>* Manchmal auch am Donnerstag.</sup>

## previously in ggg-<s>v</s>r:

### Themenabende

**Copyright vs Copyleft** [Linkdump](https://codeberg.org/GaosGombudaGlub/Veranstaltungen/src/branch/main/Linkdumps/Copyright-vs-Copyleft.md)

**Workshop: Photovoltaik Kleinkraftwerke** [Skript/Infomaterial](https://codeberg.org/GaosGombudaGlub/Veranstaltungen/src/branch/main/Vortr%C3%A4ge/PV%20Vortrag.pdf)

### Filme & Video
**The Internet's Own Boy** [Wikipedia](https://en.wikipedia.org/wiki/The_Internet%27s_Own_Boy) | [Stream auf archive.org](https://archive.org/details/TheInternetsOwnBoyTheStoryofAaronSwartzHD)

**RIP! A Remix Manifesto** [Wikipedia](https://de.wikipedia.org/wiki/RiP!:_A_Remix_Manifesto) | [Stream](https://www.nfb.ca/film/rip_a_remix_manifesto/) | [archive.org](https://archive.org/details/RipRemixManifesto)

**37c3 Rudelgucken** [Streams](https://media.ccc.de/c/37c3)