# Copyright vs Copyleft
16.5. 19:00 @ CyBerg

tl;dr gespräche zu urheberrecht, lizenzen & workarounds

Seit es das Internet gibt, tobt der Kampf zwischen Copyright-Mafia und Remixern, Pirat:innen und Freigeistern. Wie ist der Stand? Segeln noch Schiffe in der Pirate Bay? Stimmen die Gerüchte, dass Musikstreamingdienste ohne Werbung nichts kosten mit den richtigen Tricks? Wer hat eigentlich gesagt das sci-hub die Kriminellen sind und nicht Verlage wie Elsevier? Sind Werbeblocker der größte Boykott in der Geschichte der Menschheit? Und schadet das alles am Ende nicht jenen, die den Content erschaffen, also Autor:innen / Schauspieler:innen / Musiker:innen?

Der Themenabend bietet die Möglichkeit, zu solchen und anderen Fragen ins Gespräch zu kommen. Natürlich kann aus rechtlichen Gründen bezüglich einzelner Themen allenfalls hypothetisch über theoretische Eventualitäten gesprochen werden. Das stellen wir mit einer eigens trainierten KI sicher^^ 🤡 lul xD

Galigrü, GGG (https://gaosgombudaglub.codeberg.page/)

Anm: Copyright = Urheberrecht

[Copyleft](https://de.wikipedia.org/wiki/Copyleft) = Das Copyleft ist eine Klausel in urheberrechtlichen Nutzungslizenzen, die den Lizenznehmer verpflichtet, jegliche Bearbeitung des Werks (z. B. Erweiterung, Veränderung) unter die Lizenz des ursprünglichen Werks zu stellen. Die Copyleft-Klausel soll verhindern, dass veränderte Fassungen des Werks mit Nutzungseinschränkungen weitergegeben werden, die das Original nicht hat. 


# linkdump

## workarounds

### adblocker
... sind grundsätzlich legal: https://netzpolitik.org/2022/urheberrecht-springer-verlag-verliert-erneut-gegen-adblock-hersteller-eyeo/

open source adblocker für diverse Browser: https://github.com/gorhill/uBlock

Android Browser mit support für Erweiterungen: https://kiwibrowser.com
Beispiel: um ublock Origin zu installieren einfach mit Kiwibrowser den chrome app store ansurfen und "Desktopseite" in den Einstellungen aktivieren. Anschließend kann bspw.  uBlock Origin installiert werden

Overlays auf Websiten ("hier Registrieren" / "Paywalls") entfernen: https://github.com/NicolaeNMV/BehindTheOverlay
### umgehen von Paywalls
https://archive.is

https://12ft.io

### Reverse Engeneering von Android Apps
Disclaimer: Verstößt definitiv gegen die AGBs aber ist für persönliche Zwecke vermutlich legal solange die App nicht weiter verbreitet wird, da Reverse Engineering für Käufer legal ist: https://www.heise.de/news/EuGH-Recht-auf-Reverse-Engineering-zur-Fehlerkorrektur-6215679.html 

Allgemeines Vorgehen: https://yasoob.me/posts/reverse-engineering-android-apps-apktool/

Beispiel eine "nicht mehr unterstützte" App wieder zum laufen zu bekommen: https://blog.rgsilva.com/reverse-engineering-and-fixing-a-streaming-app/

Beispiel Spotify-App: https://medium.com/@tarunyadav83333/how-i-removed-ads-from-the-spotify-app-ac2191adf8ba

Geht sogar "bequem" auf dem eigenen Smartphone... https://www.youtube.com/watch?v=XsGCculv-A4
### alternative frontends
Leitet Inhalte von Websites wie YT, TikTok, Reddit & co auf datensparsame alternative werbungsfreie Webdienste :https://libredirect.codeberg.page

Community einer alternative Instagram-App https://www.reddit.com/r/Instander/

### alternative zu google suche
https://stract.com

### fediverse
dezentrales Netzwerk verschiedener Kommunikationsstools. u.a.:

"Lemmy" statt Reddit https://en.wikipedia.org/wiki/Lemmy_(social_network)

"Mastodon" statt Twitter/X https://de.wikipedia.org/wiki/Mastodon_(Soziales_Netzwerk)

"PeerTube" statt Youtube https://en.wikipedia.org/wiki/PeerTube

"Matrix" statt andere unsichere Messenger https://matrix.org -> Client für Matrix "Element": https://element.io



### DNS-Sperren umgehen
DNS-Sperren: https://netzpolitik.org/2021/clearingstelle-urheberrecht-im-internet-die-rueckkehr-der-netzsperren/

... erwischen auch sci-hub: https://netzpolitik.org/2024/netzsperre-fuer-wissenschaft-groesste-deutsche-provider-blockieren-sci-hub/

Lösung: alternative DNS-Server nutzen!

Neben Google (8.8.8.8) und Cloudflare (1.1.1.1) gibt es eine sinnvolle Alternative: https://www.opennic.org
### News aus der filesharing szene
News v.a. zu Privatsphäre & Torrents: https://torrentfreak.com

News (manchmal etwas clickbaity): https://tarnkappe.info
### verhindern von DNS-Leaks
Test und diverse Anleitungen: https://www.dnsleaktest.com

checken von DNS-Leaks bei der Verwendung von Torrentprogrammen: https://ipleak.net

## Beispiel Sci-Hub
https://de.wikipedia.org/wiki/Sci-Hub

https://www.laborjournal.de/rubric/hintergrund/hg/hg_21_03_02.php
## musik
### creative commons musikdatenbank (GEMAfrei):
https://www.jamendo.com
### über bandcamp (enshittification?):
https://www.deutschlandfunkkultur.de/bandcamp-songtradr-verkauf-musik-streaming-100.html

https://groove.de/2023/11/02/bandcamp-united-klage-gegen-songtradr-und-epic-games/

https://jacobin.com/2023/12/bandcamp-music-streaming-sale-tech-workers-union-layoffs-organizing
### Artists über diy vertrieb & urheberrecht
http://www.monofueralle.de/urheberrecht.html

http://www.monofueralle.de/socialmedia.html
## big tech & Monopole

### Monopolisierung der Techbranche
Buch darüber, wie Techunternehmen zu Monopolen werden konnten: https://en.wikipedia.org/wiki/Chokepoint_Capitalism
### enshittification
https://en.wikipedia.org/wiki/Enshittification
### Recherche zu AI generierten "Artikeln"
https://futurism.com/advon-ai-content
### Amazon
https://pluralistic.net/2024/03/01/managerial-discretion/#junk-fees

https://pluralistic.net/2023/11/06/attention-rents/
### Elsevier
https://www.deutschlandfunkkultur.de/debatte-um-wissenschaftsverlage-deutschland-vs-elsevier-100.html


